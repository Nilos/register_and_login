Symfony Standard Edition
========================

**WARNING**: This distribution does not support Symfony 4. See the
[Installing & Setting up the Symfony Framework][15] page to find a replacement
that fits you best.

Welcome to the Symfony Standard Edition - a fully-functional Symfony
application that you can use as the skeleton for your new applications.

For details on how to download and get started with Symfony, see the
[Installation][1] chapter of the Symfony Documentation.

Aide pour créer ce projet :
- https://symfony.com/doc/3.4/security/form_login_setup.html
- https://symfony.com/doc/current/security/entity_provider.html
- https://symfony.com/doc/current/doctrine/registration_form.html